# Collect log 
Start-Transcript -Path C:\Temp\Install-JPlang.txt -Append
#Check "UseWUServer" value
$UseWUServer = Get-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "UseWUServer" | Select-Object -ExpandProperty UseWUServer
#
# Set Value to 0 to enable FoD updates (features on Demand)
Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "UseWUServer" -Value 0
Restart-Service "Windows Update"
#
#Check if ja-JP Appxpackage already exists. if yes then remove so it can be installed later step
$JPappxpackage = Get-AppxPackage -AllUsers | Where-Object Name -Like *LanguageExperiencePackja-JP*
if ($JPappxpackage) {
    Write-Host "Removing old ja-JP Lang AppxPackage and Installing new..."
    $JPappxpackage | Remove-AppxPackage -Allusers
}
else { Write-Host "ja-JP LXP does not exist. Installing ... " }
#
# Install ja-JP lxp appxpackage after removing old one or if it doesnt exist
Add-AppxProvisionedPackage -online -PackagePath $PSScriptRoot\LanguageExperiencePack.ja-JP.Neutral.appx -LicensePath $PSScriptRoot\License.xml
#
#Install Japense Language Pack FoD Basic Language pack using Dism Online
DISM.exe /Online /Add-Capability /CapabilityName:Language.Basic~~~ja-JP~0.0.1.0 /NoRestart
#
# Set value of "UseWUServer" property back to original and Restart Windows update
Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU" -Name "UseWUServer" -Value $UseWUServer
Restart-Service "Windows Update"
#
# Install AppxPackage for current User
$p = (Get-AppxPackage | Where-Object Name -Like *LanguageExperiencePackja-JP).InstallLocation
#if appx path is not defined: $p = "C:\Program Files\WindowsApps\Microsoft.LanguageExperiencePackja-JP_19038.0.1.0_neutral__8wekyb3d8bbwe"
Add-AppxPackage -Register -Path "$p\AppxManifest.xml" -DisableDevelopmentMode
#
# Set PSlockdown to full language mode temoporarily to allow launguage settings to appear in Language Settings
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\SESSION MANAGER\Environment' -Name "__PSLockdownPolicy" -Value 8
#
# Set language to appear in settings (opens is new PS session)
$EnableLanguageinSettings = {
    $OSLanguages = (Get-WmiObject -Class Win32_OperatingSystem -Namespace root\CIMV2).MUILanguages
    $userlanguagelist = Get-WinUserLanguageList
    foreach ($OSLanguage in $OSLanguages) {
        $userlanguagelist.add($OSLanguage)
    }
    Set-WinUserLanguageList $userlanguagelist -force
    # Set PSlockdown back to Constrained Language Mode
    Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\SESSION MANAGER\Environment' -Name "__PSLockdownPolicy" -Value 4
}
Start-Process Powershell "-nologo -noprofile -command $EnableLanguageinSettings"
Stop-Transcript
