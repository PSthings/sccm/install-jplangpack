# Install-JPLangPack

### This script is for SCCM/MECM to make Japanese Language pack available in Software center.
Due to `UseWUServer` registry value being set to `1`, users cannot install new LXP based packages on Windows 10 v20H2+ 
This script resolves the problem by temporarily setting the value to `0` and install LXP .appx based language pack included.
It also installs Basic Typing FoD using online `dism` 

`Detect_Script.ps1` is used for detection method.
`Uninstall-JPLang.ps1` is used to uninstall language pack




