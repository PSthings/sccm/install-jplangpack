# Detect if ja-JP Basic Writing IME capability is installed
$JPBasicWriting = (Get-WindowsCapability -Online | Where-Object name -like *Language.Basic~~~ja-JP~0.0.1.0*).State
if ($JPBasicWriting -eq "Installed") {
    Write-Host "Ja-JP Basic Typing FoD package found! Uninstalling..."
    DISM.exe /Online /Remove-Capability /CapabilityName:Language.Basic~~~ja-JP~0.0.1.0 /NoRestart
}
else {
    Write-Host "Ja-JP Basic Typing FoD package not found. Continuing..."
}
#
#Detect if ja-JP LXP AppxPackage is Installed
$JPLXPAPPX = (Get-AppxPackage -Name *LanguageExperiencePackja-JP*).Name
if ($JPLXPAPPX -eq "Microsoft.LanguageExperiencePackja-JP") {
    Write-Host "Microsoft.LanguageExperiencePackja-JP package found! Uninstalling..."
    Get-AppxPackage -AllUsers | Where-Object Name -Like *LanguageExperiencePackja-JP* | Remove-AppxPackage -Allusers
}
else {
    Write-Host "Ja-JP AppxPackage does not exist. Exiting..."
}
# Remove ja-JP from bottom action bar
# Set PSlockdown to full language mode temoporarily to allow launguage settings to appear in Language Settings
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\SESSION MANAGER\Environment' -Name "__PSLockdownPolicy" -Value 8
$EnableLanguageinSettings = {
    $LangList = Get-WinUserLanguageList
    $MarkedLang = $LangList | Where-Object LanguageTag -eq "ja"
    $LangList.Remove($MarkedLang)
    Set-WinUserLanguageList $LangList -Force
    # Set PSlockdown back to Constrained Language Mode
    Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\SESSION MANAGER\Environment' -Name "__PSLockdownPolicy" -Value 4
}
Start-Process Powershell "-nologo -noprofile -command $EnableLanguageinSettings"